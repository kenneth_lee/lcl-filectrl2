unit DynTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, FileCtrl2,
  StdCtrls, ComCtrls;

type

  { TDtest }

  TDtest = class(TForm)
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
  private
    { private declarations }
  public
  //  ComboBox: TFileListBox;
    ListBox: TFileTabControl;
    FileTab: TFileTabControl;
    procedure ControlChange(Sender: TObject);
    { public declarations }
  end; 

var
  Dtest: TDtest;

implementation

uses
ExtCtrls,
FileUtil;

{ TDtest }

procedure TDtest.FormCreate(Sender: TObject);

   procedure PullCom(vf_obj: TControl);
begin
   with vf_obj do
      begin
//       Case InheritsFrom(ClassType) of
//       TFileTabControl:
//       if InheritsFrom(TFileTabControl)then Align:= altop
//        else if InheritsFrom(TFileListBox)then Align:= albottom
//        else Align:= alclient;
        
       Parent := Dtest;
       Enabled:= true;
       Mask := '*';
       if InheritsFrom(TFileTabControl)then Height := 60;
       Visible:= true;
       Show;
      end;
end;

begin
  FileTab:= TFileTabControl.Create(self);
  PullCom(FileTab);
  FileTab.TabPosition:= tpbottom;
  with FileTab do
    begin
       FileType := [ftDirectory];
       Directory := '/';
       Align:= altop;
       OnChange := @TabControl1Change;
    end;
    
  ListBox:= TFileTabControl.Create(self);
  PullCom(ListBox);
   with ListBox do
    begin
      Align:= alclient;// alleft;
     // Width := 140;
      TabWidth := 160;
      FileType := [ftNormal];
      OnChange := @ControlChange;
      TabPosition:= tpRight;
      Directory := FileTab.FileName;
      UpdateFileList;
    end;
end;

procedure TDtest.ControlChange(Sender: TObject);
begin
  Dtest.Caption := ListBox.Tabs[ListBox.TabIndex] +' ControlChange';
end;

procedure TDtest.TabControl1Change(Sender: TObject);
begin
   Dtest.Caption :=  FileTab.Tabs[FileTab.TabIndex] +  ' TabControl1Change';
   ListBox.Directory := FileTab.Tabs[FileTab.TabIndex];
   ListBox.UpdateFileList;
end;

initialization
  {$I dyntest.lrs}

end.
//Author
//Kenny D Lee
