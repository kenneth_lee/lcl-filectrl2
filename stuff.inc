

procedure {$I defile.inc}.UpdateFileList;
const
  AttrNotNormal = faReadOnly or
                  faHidden or
                  faSysFile or
                  faVolumeID or
                  faDirectory or
                  faArchive;
var
  Info: TSearchRec;

  function FileTypeToFileAttribute(FileType: TFileType): LongInt;
  const
    FileTypeToAttrMap: array[TFileAttr] of LongInt =
    (
 { ftReadOnly  } faReadOnly,
 { ftHidden    } faHidden,
 { ftSystem    } faSysFile,
 { ftVolumeID  } faVolumeId,
 { ftDirectory } faDirectory,
 { ftArchive   } faArchive,
 { ftNormal    } 0
    );
  var
    Iter: TFileAttr;
  begin
    Result := 0;
    for Iter := Low(TFileAttr) to High(TFileAttr) do
      if Iter in FileType then
        Result := Result or FileTypeToAttrMap[Iter];
  end;

begin
  if [csloading, csdestroying] * ComponentState <> [] then Exit;
  {$ifdef TabControl}Tabs{$else}Items{$endif}.Clear;
  if FileType <> [] then
  begin
    if FindFirstUTF8(
      IncludeTrailingPathDelimiter(FDirectory)+AllDirectoryEntriesMask, FileTypeToFileAttribute(FileType), Info) = 0
    then
      repeat
        if MatchesMaskList(Info.Name,Mask) then
        begin //(file || dir) output
            if (ftDirectory in FileType) and ((Info.Attr and faDirectory) > 0 ) then
              {$ifdef TabControl}Tabs{$else}Items{$endif}.Add(DirectorySeparator+Info.Name)
            else if (ftNormal in FileType) and not ((Info.Attr and faDirectory) > 0) then
              {$ifdef TabControl}Tabs{$else}Items{$endif}.Add(Info.Name)



         { //both (file && dir) or nothing output's
          if (ftNormal in FileType) or ((Info.Attr and AttrNotNormal) > 0) then
          begin
            if (Info.Attr and faDirectory) > 0 then
              {$ifdef TabControl}Tabs{$else}Items{$endif}.Add(DirectorySeparator+Info.Name)
            else
              {$ifdef TabControl}Tabs{$else}Items{$endif}.Add(Info.Name);
          end;}
        end;
      until FindNextUTF8(Info) <> 0;
    FindCloseUTF8(Info);
  end;
  UpdateSelectedFileName;
end;

procedure {$I defile.inc}.Click;
begin
  inherited Click;
  UpdateSelectedFileName;
end;

procedure {$I defile.inc}.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited KeyUp(Key, Shift);
  UpdateSelectedFileName;
end;


procedure {$I defile.inc}.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  UpdateSelectedFileName;
end;

procedure {$I defile.inc}.Loaded;
begin
  inherited Loaded;
  UpdateFileList;
end;

procedure {$I defile.inc}.SetFileType(const AValue: TFileType);
begin
  if FFileType=AValue then exit;
  FFileType := AValue;
  UpdateFileList;
end;

procedure {$I defile.inc}.SetDirectory(const AValue: String);
begin
  if FDirectory=AValue then exit;
  FDirectory := AValue;
  UpdateFileList;
end;

function {$I defile.inc}.MaskIsStored: boolean;
begin
  Result:=(FMask<>AllDirectoryEntriesMask);
end;

procedure {$I defile.inc}.SetDrive(const AValue: Char);
begin
  if FDrive=AValue then exit;
  FDrive := AValue;
  // ToDo: change to current directory of drive
  UpdateFileList;
end;

procedure {$I defile.inc}.SetMask(const AValue: String);
begin
  if FMask = AValue then exit;
  FMask := AValue;
  UpdateFileList;
end;

Function {$I defile.inc}.IndexOf(const S: string): Integer;
begin
  Result:=0;
  While (Result<{$ifdef TabControl}Tabs{$else}Items{$endif}.Count) and
  (CompareText({$ifdef TabControl}Tabs{$else}Items{$endif}[Result],S)<>0) do inc(Result);
  if Result={$ifdef TabControl}Tabs{$else}Items{$endif}.Count then Result:=-1;
end;

procedure {$I defile.inc}.SetFileName(const AValue: String);
var
  i: Integer;
begin
  i:={$ifdef TabControl}Tabs{$else}Items{$endif}.IndexOf(AValue);
  if i<>{$ifdef TabControl}TabIndex{$else}ItemIndex{$endif} then begin
    {$ifdef TabControl}TabIndex{$else}ItemIndex{$endif}:=i;
    UpdateSelectedFileName;
  end;
end;

procedure {$I defile.inc}.UpdateSelectedFileName;
var
  i: Integer;
begin
  i:={$ifdef TabControl}TabIndex{$else}ItemIndex{$endif};
  if (i<0) or (i>={$ifdef TabControl}Tabs{$else}Items{$endif}.Count) then
    FFileName := ''
  else begin
     FFileName := {$ifdef TabControl}Tabs{$else}Items{$endif}[i];
     if (FFileName[1]=DirectorySeparator) then
      FFileName := FDirectory + {$ifdef TabControl}Tabs{$else}Items{$endif}[i]
     else FFileName := FDirectory+DirectorySeparator+{$ifdef TabControl}Tabs{$else}Items{$endif}[i];
  end;
  DoChangeFile;
end;

procedure {$I defile.inc}.DoChangeFile;
begin
  if FFilename=FLastChangeFileName then exit;
  FLastChangeFileName:=FFilename;
  If Assigned(FOnChange) then FOnChange(Self);
end;

constructor {$I defile.inc}.Create(TheOwner: TComponent);
var
  FileDrive: String;
begin
  inherited Create(TheOwner);
  //Initializes the Mask property.
  FMask := AllDirectoryEntriesMask;
  //Initializes the FileType property.
  FFileType := [ftNormal];
  //Initializes the Directory and Drive properties to the current directory.
  FDirectory := GetCurrentDir;
  FileDrive := ExtractFileDrive(FDirectory);
  if FileDrive<>'' then
    FDrive:=FileDrive[1]
  else
    FDrive:=' ';
  //Initializes the MultiSelect property.
  {$if defined(ListBox) or defined(TabControl)}
   MultiSelect := False;
  {$endif}
 // ;
  //Fills the list box with all the files in the directory.
  UpdateFileList;
//  {$ifdef TabControl}TabIndex{$else}ItemIndex{$endif} := 0;
  //Initializes the Sorted property.
   //SetSorted(True);
end;

destructor {$I defile.inc}.Destroy;
begin
  inherited Destroy;
end;

{$if not defined(ListBox) or not defined(ComboBox)}
procedure {$I defile.inc}.SetSorted(const AValue: boolean);
var
Sort:TStringlist;
begin
    if AValue <> FSorted then
     FSorted:= AValue;
    if AValue <> false then
    begin
      Sort:= TStringlist.Create;
      Sort.AddStrings({$ifdef TabControl}Tabs{$else}Items{$endif});
      Sort.Sort;
      {$ifdef TabControl}Tabs{$else}Items{$endif}.Assign(Sort);
      FreeAndNil(Sort);
    end;
end;
{$endif}

{$ifdef CheckGroup}

function TFileCheckGroup.GetItemIndex: integer;
begin
  Result := FItemIndex;
end;

procedure TFileCheckGroup.SetItemIndex(const AValue: integer);
begin
  if (AValue <> -1) and (AValue<Items.Count) then
      FItemIndex := AValue;
end;

{$endif}




