
 //   TFileListBox, TFileComboBox, TFileRadioGroup, TFileTabControl, TFileGroupBox

//    TFileListDef = class(TFileList)
  private
    FDrive: Char;
    FDirectory: String;
    FFileName: String;
    FFileType: TFileType;
    FMask: String;
    FOnChange: TNotifyEvent;
    FLastChangeFileName: string;
  //  procedure Assign(Source: TPersistent); override;
{$if not defined(ListBox) or not defined(ComboBox)}
    FSorted: boolean;
{$ifdef CheckGroup}
    FItemIndex:integer;
    function GetItemIndex: integer;
    procedure SetItemIndex(const AValue: integer);
{$endif}
    procedure SetSorted(const AValue: boolean);  overload;
{$endif}
    function MaskIsStored: boolean;
    procedure SetDirectory(const AValue: String);
    procedure SetDrive(const AValue: Char);
    procedure SetFileName(const AValue: String);
    procedure SetFileType(const AValue: TFileType);
    procedure SetMask(const AValue: String);
    procedure UpdateSelectedFileName;
  protected
    procedure DoChangeFile; virtual;
    procedure Click; override;
    procedure Loaded; override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    Function  IndexOf(const S: string): Integer;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateFileList; virtual;
//    SearchAllFilesInPath
  public
{$if not defined(ListBox) or not defined(ComboBox)}
    property Sorted : boolean Read FSorted Write SetSorted default false;
{$ifdef CheckGroup}
    property ItemIndex: integer Read GetItemIndex Write SetItemIndex default 0;
{$endif}
{$endif}
  published
    property Drive: Char Read FDrive Write SetDrive default ' ';
    property Directory: String Read FDirectory Write SetDirectory;
    property FileName: String Read FFileName Write SetFileName;
    property FileType: TFileType Read FFileType Write SetFileType default [ftNormal];
    property Mask: String Read FMask Write SetMask stored MaskIsStored;
  {$ifndef TabControl}
    property OnChange: TNotifyEvent Read FOnChange Write FOnChange;
  {$endif}
    property OnDblClick;
//    property OnKeyUp;
  end;
    //InheritsFrom()
