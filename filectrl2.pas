

{
 /***************************************************************************
                               filectrl.pp Revision
                               -----------
                             Component Library File Controls
                   Revision  : Kenneth Dwayne Lee sept 1, 2011



 ***************************************************************************/

 *****************************************************************************
 *                                                                           *
 *  This file origin Lazarus Component Library (LCL)                 *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL, included in this distribution,        *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************

This unit contains file and directory controls and supporting handling functions.
}

unit filectrl2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, ExtCtrls, StdCtrls, ComCtrls, FileUtil, Masks;

Type

  TFileAttr = (ftReadOnly, ftHidden, ftSystem, ftVolumeID, ftDirectory, ftArchive, ftNormal);

  TFileType = set of TFileAttr;

{$define ListBox}TFileListBox2 = class(TListBox){$I stuffintf.inc}{$undef ListBox}
{$define ComboBox}TFileComboBox = class(TComboBox){$I stuffintf.inc}{$undef ComboBox}
{$define RadioGroup}TFileRadioGroup = class(TRadioGroup){$I stuffintf.inc}{$undef RadioGroup}
{$define CheckGroup}TFileCheckGroup = class(TCheckGroup){$I stuffintf.inc}{$undef CheckGroup}
{$define TabControl}TFileTabControl = class(TTabControl){$I stuffintf.inc}{$undef TabControl}

procedure Register;

implementation

 {$define ListBox}{$I stuff.inc}{$undef ListBox}
 {$define ComboBox}{$I stuff.inc}{$undef ComboBox}
 {$define RadioGroup}{$I stuff.inc}{$undef RadioGroup}
 {$define CheckGroup}{$I stuff.inc}{$undef CheckGroup}
 {$define TabControl}{$I stuff.inc}{$undef TabControl}
 
procedure Register;
begin
  RegisterComponents('file',[TFileListBox2, TFileComboBox, TFileRadioGroup, TFileTabControl]);
end;


end.
